<div class="container" align="left">
<h1>Log in</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="User">User</label>
        <input required type="text" class="form-control" id="User" placeholder="Enter a User" name="User">
    </div>

    <div class="form-group">
        <label for="Password">Password</label>
        <input required type="password" class="form-control" id="Password" placeholder="Enter a Password" name="Password">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

</form>
</div>