<div class="container" align="left">
<h1>Edit task</h1>

<form method='post' action='#'>
    <div class="form-group">
        <label for="Name">Name</label>
        <input type="text" class="form-control" id="Name" placeholder="Enter a Name" name="Name" value ="<?php if (isset($task["Name"])) echo $task["Name"];?>">
    </div>

    <div class="form-group">
        <label for="Email">Email</label>
        <input type="text" class="form-control" id="Email" placeholder="Enter a Email" name="Email" value ="<?php if (isset($task["Email"])) echo $task["Email"];?>">
    </div>

    <div class="form-group">
        <label for="description">Description</label>        
        <textarea class="form-control" id="Description" name="Description" cols="40" rows="3"><?php if (isset($task["Description"])) echo $task["Description"];?></textarea></p>
    </div>

    <div class="form-group">
        <label for="Done">Done</label>
        <input type="checkbox" id="Done" name="Done"
            <?php echo $task['Done'] == 1 ? 'checked' : '' ?>
        >
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>