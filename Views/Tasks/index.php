<div align="left">
<h1>Tasks</h1>

<p>
<a href="/tasks/create/" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new task</a>
</p>
</div>

<div class="col-md-12 centered">
    <table id="Table" class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Description</th>
            <th>Status</th>
            <?php if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) : ?>
            <th class="text-center">Action</th>
            <?php endif; ?> 
            
              
        </tr>
        </thead>
        <?php
        foreach ($tasks as $task)
        {
            echo '<tr>';
            echo "<td>" . $task['ID'] . "</td>";
            echo "<td>" . $task['Name'] . "</td>";
            echo "<td>" . $task['Email'] . "</td>";
            echo "<td>" . $task['Description'] . "</td>";
            echo "<td>";
            echo "<p>";
            echo $task['Done'] == 1 ? 'Готово' : 'Не готово';
            echo '<p>';
            if ($task['Flag'] == 1) {
                echo '<p><small>Изменено администратором</small></p>';
            }
            echo "</td>";
            if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {
                echo "<td class='text-center'><a class='btn btn-info btn-sm' href='/tasks/edit/" . $task["ID"] . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/tasks/delete/" . $task["ID"] . "' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
<script>
$(document).ready(function() {
    $('#Table').DataTable({"pageLength": 3});
} );
</script>

