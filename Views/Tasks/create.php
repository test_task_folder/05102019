<div class="controller" align="left">
<h1>Create task</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="Name">Name</label>
        <input required type="text" class="form-control" id="Name" placeholder="Enter a Name" name="Name">
    </div>

    <div class="form-group">
        <label for="Email">Email</label>
        <input required type="email" class="form-control" id="Email" placeholder="Enter a Email" name="Email">
    </div>

    <div class="form-group">
        <label for="Description">Description</label>
        <!-- <input type="text" class="form-control" id="Description" placeholder="Enter a Description" name="Description"> -->
        <textarea required class="form-control" id="Description" placeholder="Enter a Description" name="Description" cols="40" rows="3"></textarea></p>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>