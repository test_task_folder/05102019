<!doctype html>
<head>
    <meta charset="utf-8">

    <title>Tasks</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet">

    <link href="starter-template.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
 
    <style>
        body {
            padding-top: 5rem;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <?php if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) : ?>
            <a class="navbar-brand" href="/tasks/logout/">Logout</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        <?php elseif (!isset($_SESSION['admin'])) : ?>
            <a class="navbar-brand" href="/tasks/login/">Login</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
    <?php endif; ?>    

</nav>

<main role="main" class="container">

    <div class="starter-template">

        <?php
        if ($_SERVER['HTTP_REFERER'] == 'http://testtask.zzz.com.ua/tasks/create/' && isset($_SESSION['create'])) {
            echo '<b>'.$_SESSION['create'].'</b>';
        }

        if ($_SERVER['HTTP_REFERER'] == 'http://testtask.zzz.com.ua/tasks/login/' && isset($_SESSION['admin'])) {
            echo '<b>login success</b>';
        }

        if ($_SERVER['HTTP_REFERER'] == 'http://testtask.zzz.com.ua/tasks/login/' && !isset($_SESSION['admin'])) {
            echo '<b>login failed</b>';
        }


        echo $content_for_layout;
        ?>

    </div>

</main>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
</body>
</html>
