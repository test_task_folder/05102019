<?php
class Task extends Model
{
    public function create($name, $email, $description)
    {
        $sql = "INSERT INTO tasks (Name, Email, Description) VALUES (:Name, :Email, :Description)";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'Name' => strip_tags($name),
            'Email' => $email,
            'Description' => htmlspecialchars($description, ENT_QUOTES, 'UTF-8')

        ]);
    }

    public function showTask($id)
    {
        $sql = "SELECT * FROM tasks WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAllTasks()
    {
        $sql = "SELECT * FROM tasks";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function edit($id, $name, $email, $description, $flag = 0, $done = 0)
    {
        $sql = "UPDATE tasks SET Name = :Name, Email = :Email, Description = :Description, Flag = :Flag, Done = :Done WHERE ID = :ID";

        $req = Database::getBdd()->prepare($sql);

        if ($done == 'on') {
            $done = 1;
        }

        return $req->execute([
            'ID' => $id,
            'Name' => $name,
            'Email' => $email,
            'Description' => htmlspecialchars($description, ENT_QUOTES, 'UTF-8'),
            'Flag' => $flag,
            'Done' => $done

        ]);
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM tasks WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
?>