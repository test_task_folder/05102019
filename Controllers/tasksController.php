<?php
class tasksController extends Controller
{
    function index()
    {   
        session_start();

        require(ROOT . 'Models/Task.php');

        $tasks = new Task();

        $d['tasks'] = $tasks->showAllTasks();
        $this->set($d);
        $this->render("index");
    }

    function create()
    {
        session_start();
        if (isset($_POST["Name"]))
        {
            require(ROOT . 'Models/Task.php');

            $task= new Task();

            if ($task->create($_POST["Name"], $_POST["Email"], $_POST["Description"]))
            {   
                
                $_SESSION['create'] = 'Задание создано';
                header("Location: /tasks");
            }
        }

        $this->render("create");
    }

    function edit($id)
    {
        session_start();

        if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {            
        } else {
            header("Location: /tasks/login");
        }

        require(ROOT . 'Models/Task.php');
        $task= new Task();

        $d["task"] = $task->showTask($id);        
        
        if (isset($_POST["Name"]))     
        {   
    
            if (!isset($_SESSION['admin'])) {       
                die;    
            }

            if ($d['task']['Flag'] != 1) {
                if (/*$d['task']['Name'] == $_POST['Name'] && $d['task']['Email'] == $_POST['Email'] && */$d['task']['Description'] == $_POST['Description']) {
                } else {
                    $flag = 1;
                }
            } else {
                $flag = 1;
            }

            if ($task->edit($id, $_POST["Name"], $_POST["Email"], $_POST["Description"], $flag, $_POST['Done']))
            {
                header("Location: /tasks/index");
            }
        }
        $this->set($d);
        $this->render("edit");
    }

    function delete($id)
    {   
        session_start();

        if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {            
        } else {
            header("Location: /tasks/login");
        }

        require(ROOT . 'Models/Task.php');

        $task = new Task();
        if ($task->delete($id))
        {
            header("Location: /tasks/index");
        }
    }

    function login()
    {
        if (isset($_POST['User']) && isset($_POST['Password'])) {
            if ($_POST['User'] == 'admin' && $_POST['Password'] == 123) {
                session_start();
                $_SESSION['admin'] = 1;
                header("Location: /tasks/index");
            }
        }
        $this->render("login");
    }

    function logout()
    {
        session_start();
        unset($_SESSION['admin']);
        header("Location: /tasks");
    }
}
?>